/*
    You have a five-quart jug, a three-quart jug, and an unlimited supply of water (but no measuring cups). How would you come up with exactly four quarts of water? Note that the jugs are oddly shaped, such that filling up exactly "half" of the jug would be impossible.
*/

// REDO (CANNOT ASSUME ANY OTHER CONTAINERS)
/*
    With a five-quart jug and a three-quart jug, you can measure:
    three quarts by filling up the three-quart jug
    five quarts by filling up the five-quart jug
    two quarts by filling up the five-quart jug, emptying the three-quart jug if there's any water in it, and pouring the five-quart into the three-quart until the three-quart is full

    So to fill up four quarts, you would measure two quarts twice.
*/
