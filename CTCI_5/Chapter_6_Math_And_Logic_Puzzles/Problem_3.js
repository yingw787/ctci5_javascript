/*
    There is an 8x8 chessboard in which two diagonally opposite corners have been cut off. You are given 31 dominos, and a single domino can cover exactly two squares. Can you use the 31 dominos to cover the entire board? Prove your answer (by providing an example or showing why it's impossible).
*/

// TODO: REVIEW
/*
    Each domino on the chessboard must cover one black square and one white square. In an 8x8 chessboard with two diagonally opposite corners cut off, there are 30 black squares and 32 white squares. Since there is not a 1:1 correlation between the number of black squares and the number of white squares, it is impossible to place dominos to fit exactly on the entire board.
*/
