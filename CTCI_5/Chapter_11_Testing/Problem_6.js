/*
    How would you test an ATM in a distributed banking system?
*/

/*
    ATM:
    - Security: hacking, spoofing, tampering
    - Ease of use: time it takes to perform ATM operations (withdrawing money, adding money, etc.)
    - Updatability: software updates, hardware updates, etc.
    - Ease of access: Adding/removing currency, movabiilty to different locations, etc.
    - Durability: weatherproofing, aging, accidents, fire, etc.

    Perform tests to see whether ATM fits all criteria as listed above, and patch tests whenever needs come up.
*/

// CORRECT NO REDO
