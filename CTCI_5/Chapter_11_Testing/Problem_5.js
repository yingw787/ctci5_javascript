/*
    How would you test a pen?
*/

/*
    Characteristics of a pen
    - Amount of ink
    - Price/Quality
    - Safety
    - Durability
    - Usability
    - Point type
    - Ink type

    Testing:
    - Durability tests: drop tests, scratch tests, crush tests, snap-off-the-page-saver-thing test
    - Amount of ink tests: draw until the ink runs out, compare to accepted results
    - Safety tests: chemical analysis of ink to ensure low toxicity
    - Quality: grip tests, likability by different consumer types, etc.
    - Usability: write on different materials to see how much ink comes out
    - Ink type tests: writing on different materials, adding in different potential solvents, etc.
*/

// CORRECT NO REDO
