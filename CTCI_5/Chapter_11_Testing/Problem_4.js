/*
    How would you load test a webpage without using any test tools?
*/

/*
    Questions:
    - Is the webpage a static web page, or dynamic?
    - What components make up the webpage?
    - What technologies make up the webpage? (client vs. server-side rendering, front-end/back-end architecture, etc.)
    - How important is performance vs. reliability (vs. other metrics)?
    - How important is a single instance of the webpage vs. all instances? (soak time)

    - Test whether web page loads on different browsers correctly (especially mobile, tablet, and desktop with differing screen resolutions)
    - Test whether all webpage components work properly
    - Test on many machines at the same time
    - Test with many tabs on the same browser
    - Test with many windows
*/

// TODO: REVIEW
/*
    BOOK SOLUTION
    Response time
    Throughput
    Resource utilization
    Maximum load system can bear

    - Virtual users
    - Write scripts utilizing many threads
    - Handle response time, data I/O programmatically
*/
