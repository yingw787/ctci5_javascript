/*
    Explain how you would design a personal financial manager (like Mint.com). THis system would connect to your bank accounts, analyze your spending habits, and make recommendations.
*/

/*
    I would design a site like Mint.com to be a collection of services.
    An authentication and user service would enable users to create accounts and securely sign into them.
    An API service would connect each of the user's financial accounts and deliver them to a processing service.
    The processing service would combine all of the financial exchanges present from the accounts and deliver them in a user-friendly manner.
    A recommendation service would be able to tell the user whether there's additional saved money per time period, or if he/she needs to cut back on spending.

    Additional complexity can be added in terms of user preferences in financial spending, manual/automated parsing and categorization of spending habits based on transaction details, inclusion of cash-based services using a camera application or manual punch-in, etc.
*/

/*
    DRAW BLOCK DIAGRAM TO SHOW HOW THE SYSTEM WOULD LOOK
*/
