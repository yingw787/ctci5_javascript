/*
    Write a function to determine the number of bits you would need to flip to convert integer A to integer B.

    EXAMPLE

    Input: 29 (11101), 15 (01111)
    Output: 2
*/

/*
    XOR the two values and count the number of ones from the result.
    // EDIT: CORRECT
*/
